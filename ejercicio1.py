#!/usr/bin/env python3
# -*- coding:utf-8 -*-

def archivop():#extracción archivo
    lineas = []
    with open("top50.csv") as file_:
        for line in file_:
            if line == 0:
                continue
            else:
                lineas.append(line.split(","))
    return lineas


def analisis1(datos):

    artistas_fin = []

    for i in datos:#extraer columna
        artistas_fin.append(i[2])

    repetidos = []
    artistas_finales = []

    for i in artistas_fin:#eliminación de datos innecesarios de la fila 1
        if i not in artistas_finales and i != "Artist.Name":
            artistas_finales.append(i)
        else:
            repetidos.append(i)

    print("los artistas son:")
    contador = 0

    for i in artistas_finales:#contador de artistas
        if i != "Artist.Name":
            print(i)
            contador += 1
    print("hay", contador, "artistas")

    almacendic = {}
    for i in repetidos:
        if not i in almacendic:
            almacendic[i] = repetidos.count(i)
    values = almacendic.values()

    x = 0
    v = ""

    for i in values:
        temp = x
        x = i
        if x > temp:
            v = x

    valor = almacendic.get(v)
    print("el artista que tiene más canciones es: ", valor)#falta agregar su función



def mediana(datos):

    datos_mediana1 = []#traspaso de datos
    for i in datos:
        datos_mediana1.append(i[7])


    datos_finales = []
    for i in datos_mediana1:#extraer datos si la primera fila
        if i != "Loudness..dB..":
            datos_finales.append(i)

    datos_finales.sort()#orden de datos
    numero_med = len(datos_finales)


def baila_resolv(datos):

    datos_bailables = []#traspaso de datos
    for i in datos:
        datos_bailables .append(i[6])

    datos_bail_fin = []
    for i in datos_bailables:#extraer datos sin la primera fila
        if i != "Danceability":
            datos_bail_fin.append(i)

    long = len(datos_bail_fin)


    datos_bail_fin.sort()
    print(datos_bail_fin)

    temp = 0
    maximos = []
    minimos = []
    for i in datos_bail_fin:#valores deben ser almacenados en diccionario para tener de key
                            #a la canción y valor al número de bailable, luego somplemente acceder
                            #usando las keys
        if temp < 3:
            minimos.append(i)
        elif temp > 46:
            maximos.append(i)
        temp += 1


    print("canciones mas bailables:")
    print("canciones menos bailables:")

if __name__ == "__main__":

    datos = archivop()
    datos_ordenados = analisis1(datos)
    med = mediana(datos)
    bailables = baila_resolv(datos)
